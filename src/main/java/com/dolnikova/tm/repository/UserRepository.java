package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;
import com.dolnikova.tm.enumeration.Role;
import com.dolnikova.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UserRepository implements IUserRepository {

    private Map<String, User> users = new LinkedHashMap<>();

    @Override
    public List<User> findAll() {
        List<User> allUsers = new ArrayList<>();
        for (Map.Entry<String, User> user : users.entrySet()) {
            allUsers.add(user.getValue());
        }
        return allUsers;
    }

    @Override
    public User findOne(String name) {
        for (Map.Entry<String, User> user : users.entrySet()) {
            if (user.getValue().getLogin().equals(name))
                return user.getValue();
        }
        return null;
    }

    @Override
    public void persist(User user) {
        users.put(user.getUserId(), user);
    }

    @Override
    public void merge(String newPassword, User user) {
        user.setPassword(HashUtil.stringToHashString(newPassword));
        users.put(user.getUserId(), user);
    }

    public void merge(String newData, User user, DataType dataType) {
        switch (dataType) {
            case LOGIN:
                user.setLogin(newData);
                break;
            case ROLE:
                if (Role.USER.toString().equalsIgnoreCase(newData)) user.setRole(Role.USER);
                if (Role.ADMIN.toString().equalsIgnoreCase(newData)) user.setRole(Role.ADMIN);
                break;
        }
        users.put(user.getUserId(), user);
    }

    @Override
    public void remove(String id) {
        users.remove(id);
    }

    @Override
    public void remove(User user) {
        users.remove(user.getUserId());
    }

    @Override
    public void removeAll() {
        users.clear();
    }

    public Map<String, User> getUsers() {
        return users;
    }

    public void setUsers(Map<String, User> users) {
        this.users = users;
    }
}
