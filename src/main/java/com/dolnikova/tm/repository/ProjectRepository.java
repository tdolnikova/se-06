package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.entity.Project;

import java.util.*;

public class ProjectRepository implements IProjectRepository {

    private Bootstrap bootstrap;

    public ProjectRepository(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();

    @Override
    public List<Project> findAll() {
        List<Project> allProjects = new ArrayList<>();
        for (Map.Entry<String, Project> project : projects.entrySet()) {
            if (project.getValue().getUserId().equals(bootstrap.getUser().getUserId())) allProjects.add(project.getValue());
        }
        return allProjects;
    }

    @Override
    public void removeAll() {
        for (Map.Entry<String, Project> project : projects.entrySet()) {
            if (project.getValue().getUserId().equals(bootstrap.getUser().getUserId())) projects.remove(project.getKey());
        }
    }

    @Override
    public void persist(Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public Project findOne(String id) {
        for (Map.Entry<String, Project> project : projects.entrySet()) {
            Project foundProject = project.getValue();
            if (id.equals(foundProject.getId())
                    && project.getValue().getUserId().equals(bootstrap.getUser().getUserId())) return foundProject;
        }
        return null;
    }

    @Override
    public void merge(String newProjectName, Project project) {
        if (project.getUserId().equals(bootstrap.getUser().getUserId())) project.setName(newProjectName);
    }

    @Override
    public void remove(String id) {
        Project projectToRemove = projects.get(id);
        if (projectToRemove.getUserId().equals(bootstrap.getUser().getUserId())) projects.remove(id);
    }

    @Override
    public void remove(Project project) {
        if (project.getUserId().equals(bootstrap.getUser().getUserId())) projects.remove(project.getId());
    }

}
