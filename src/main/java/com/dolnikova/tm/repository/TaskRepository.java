package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.entity.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository implements ITaskRepository {

    private Bootstrap bootstrap;

    public TaskRepository(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private static LinkedHashMap<String, Task> tasks = new LinkedHashMap<>();

    @Override
    public List<Task> findAll() {
        List<Task> allTasks = new ArrayList<>();
        for (Map.Entry<String, Task> task : tasks.entrySet()) {
            if (task.getValue().getUserId().equals(bootstrap.getUser().getUserId())) allTasks.add(task.getValue());
        }
        return allTasks;
    }

    @Override
    public void removeAll() {
        for (Map.Entry<String, Task> project : tasks.entrySet()) {
            if (project.getValue().getUserId().equals(bootstrap.getUser().getUserId())) tasks.remove(project.getKey());
        }
    }

    @Override
    public void persist(Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public Task findOne(String id) {
        for (Map.Entry<String, Task> task : tasks.entrySet()) {
            Task foundTask = task.getValue();
            if (id.equals(foundTask.getId())
                    && task.getValue().getUserId().equals(bootstrap.getUser().getUserId())) return foundTask;
        }
        return null;
    }

    @Override
    public void merge(String id, Task task) {
        if (task.getUserId().equals(bootstrap.getUser().getUserId())) {
            task.setId(id);
            tasks.put(id, task);
        }
    }

    @Override
    public void remove(String id) {
        Task taskToRemove = tasks.get(id);
        if (taskToRemove.getUserId().equals(bootstrap.getUser().getUserId())) tasks.remove(id);
    }

    @Override
    public void remove(Task task) {
        if (task.getUserId().equals(bootstrap.getUser().getUserId())) tasks.remove(task.getId());
    }

    public List<Task> getTasksByProjectId(String projectId) {
        List<Task> allTasks = findAll();
        List<Task> projectTasks = new ArrayList<>();
        for (Task task : allTasks) {
            if (task.getProjectId().equals(projectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

}
