package com.dolnikova.tm.util;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;

public class HashUtil {

    public static String stringToHashString(String string) {
        String hashString = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(string.getBytes());
            byte[] bytes = md.digest();
            hashString = DatatypeConverter.printHexBinary(bytes);
        } catch (Exception e) {e.printStackTrace();}
        return hashString;
    }

}
