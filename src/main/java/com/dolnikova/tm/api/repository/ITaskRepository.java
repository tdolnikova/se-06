package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends Repository<Task> {

    @Override
    List<Task> findAll();

    @Override
    Task findOne(String name);

    @Override
    void persist(Task task);

    @Override
    void merge(String newName, Task task);

    @Override
    void remove(String id);

    @Override
    void remove(Task task);

    @Override
    void removeAll();
}
