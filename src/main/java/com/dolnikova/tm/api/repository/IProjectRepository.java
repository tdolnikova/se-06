package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends Repository<Project> {

    @Override
    List<Project> findAll();

    @Override
    Project findOne(String name);

    @Override
    void persist(Project project);

    @Override
    void merge(String newName, Project project);

    @Override
    void remove(String id);

    @Override
    void remove(Project project);

    @Override
    void removeAll();
}
