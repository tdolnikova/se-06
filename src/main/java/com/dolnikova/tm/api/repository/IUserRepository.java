package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.User;

import java.util.List;

public interface IUserRepository extends Repository<User> {

    @Override
    List<User> findAll();

    @Override
    User findOne(String name);

    @Override
    void persist(User entity);

    @Override
    void merge(String newName, User entity);

    @Override
    void remove(String id);

    @Override
    void remove(User entity);

    @Override
    void removeAll();
}
