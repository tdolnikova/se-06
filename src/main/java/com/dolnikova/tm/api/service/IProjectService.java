package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Project;

import java.util.List;

public interface IProjectService extends Service<Project> {

    @Override
    List<Project> findAll();

    @Override
    Project findOne(String name);

    @Override
    void persist(Project project);

    @Override
    void merge(String newName, Project entityToMerge);

    @Override
    void remove(Project project);

    @Override
    void removeAll();
}
