package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.User;

import java.util.List;

public interface IUserService extends Service<User> {

    @Override
    List<User> findAll();

    @Override
    User findOne(String name);

    @Override
    void persist(User entity);

    @Override
    void merge(String newName, User entityToMerge);

    @Override
    void remove(User entity);

    @Override
    void removeAll();
}
