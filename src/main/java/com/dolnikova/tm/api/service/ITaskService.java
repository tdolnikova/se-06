package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Task;

import java.util.List;

public interface ITaskService extends Service<Task> {

    @Override
    List<Task> findAll();

    @Override
    Task findOne(String name);

    @Override
    void persist(Task task);

    @Override
    void merge(String newName, Task entityToMerge);

    @Override
    void remove(Task task);

    @Override
    void removeAll();
}
