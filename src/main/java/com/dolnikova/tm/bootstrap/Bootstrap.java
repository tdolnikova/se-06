package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.command.exit.ExitCommand;
import com.dolnikova.tm.command.help.HelpCommand;
import com.dolnikova.tm.command.project.*;
import com.dolnikova.tm.command.task.*;
import com.dolnikova.tm.command.user.*;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.Role;
import com.dolnikova.tm.exception.CommandCorruptException;
import com.dolnikova.tm.repository.ProjectRepository;
import com.dolnikova.tm.repository.TaskRepository;
import com.dolnikova.tm.repository.UserRepository;
import com.dolnikova.tm.service.ProjectService;
import com.dolnikova.tm.service.TaskService;
import com.dolnikova.tm.service.UserService;

import java.util.*;

public class Bootstrap {

    private final static Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    public final static Scanner scanner = new Scanner(System.in);
    private ProjectRepository projectRepository = new ProjectRepository(this);
    private TaskRepository taskRepository = new TaskRepository(this);
    private UserRepository userRepository = new UserRepository();
    private UserService userService = new UserService(userRepository);
    private TaskService taskService = new TaskService(taskRepository);
    private ProjectService projectService = new ProjectService(projectRepository, taskService);
    private User admin;
    private User user;

    public void init() {
        admin = new User(Role.ADMIN);
        admin.setLogin("admin");
        admin.setPassword("admin");
        userService.persist(admin);
        user = new User(Role.USER);
        user.setLogin("user");
        user.setPassword("user");
        userService.persist(user);
        try {
            registry(new HelpCommand());
            registry(new ExitCommand());

            registry(new UserAuthCommand(this));
            registry(new UserChangePasswordCommand(this));
            registry(new UserEditProfileCommand(this));
            registry(new UserFindProfileCommand(this));
            registry(new UserRegisterCommand(this));
            registry(new UserSignOutCommand(this));

            registry(new ProjectFindAllCommand(this));
            registry(new ProjectFindOneCommand(this));
            registry(new ProjectMergeCommand(this));
            registry(new ProjectPersistCommand(this));
            registry(new ProjectRemoveAllCommand(this));
            registry(new ProjectRemoveCommand(this));

            registry(new TaskFindAllCommand(this));
            registry(new TaskFindOneCommand(this));
            registry(new TaskMergeCommand(this));
            registry(new TaskPersistCommand(this));
            registry(new TaskRemoveAllCommand(this));
            registry(new TaskRemoveCommand(this));

            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        commands.put(cliCommand, command);
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    public static List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public static Scanner getScanner() {
        return scanner;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public UserService getUserService() {
        return userService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public User getAdmin() {
        return admin;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
