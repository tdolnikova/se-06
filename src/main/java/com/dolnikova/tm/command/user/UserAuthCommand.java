package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.User;

public class UserAuthCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public UserAuthCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void setBootstrap(Bootstrap bootstrap) {
        super.setBootstrap(bootstrap);
    }

    @Override
    public String command() {
        return Constant.USER_AUTH;
    }

    @Override
    public String description() {
        return Constant.USER_AUTH_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[AUTHORIZATION]");
        System.out.println(Constant.REG_ENTER_LOGIN);
        String login = "";
        while (login.isEmpty()) {
            login = Bootstrap.scanner.nextLine();
        }
        System.out.println(Constant.REG_ENTER_PASSWORD);
        String password = "";
        while (password.isEmpty()) {
            password = Bootstrap.scanner.nextLine();
        }
        User user = bootstrap.getUserService().findOne(login);
        if (user == null) {
            System.out.println(Constant.REG_USER_NOT_FOUND);
            return;
        }
        bootstrap.setUser(user);
        System.out.println(Constant.REG_USER_SUCCESSFUL_LOGIN);
    }

    @Override
    public boolean isSecure() {
        return bootstrap.getUser().getUserId().isEmpty();
    }
}
