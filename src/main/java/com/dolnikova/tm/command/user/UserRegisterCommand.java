package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.Role;
import com.dolnikova.tm.util.HashUtil;

import java.util.UUID;

public class UserRegisterCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public UserRegisterCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void setBootstrap(Bootstrap bootstrap) {
        super.setBootstrap(bootstrap);
    }

    @Override
    public String command() {
        return Constant.USER_REG;
    }

    @Override
    public String description() {
        return Constant.USER_REG_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[REGISTRATION]");
        System.out.println(Constant.REG_ENTER_LOGIN);
        String login = "";
        while (login.isEmpty()) {
            login = Bootstrap.scanner.nextLine();
        }
        System.out.println(Constant.REG_ENTER_PASSWORD);
        String password = "";
        while (password.isEmpty()) {
            password = Bootstrap.scanner.nextLine();
        }
        if (bootstrap.getUserService().findOne(login) == null) {
            User newUser = new User(Role.USER);
            newUser.setLogin(login);
            newUser.setPassword(HashUtil.stringToHashString(password));
            newUser.setUserId(UUID.randomUUID().toString());
            bootstrap.getUserService().persist(newUser);
            System.out.println(Constant.REG_USER_REGISTERED);
            System.out.println(bootstrap.getUserService().findAll().toString());
        } else {
            System.out.println(Constant.REG_USER_EXISTS);
        }

    }

    @Override
    public boolean isSecure() {
        return (bootstrap.getUser().getUserId().isEmpty());
    }
}
