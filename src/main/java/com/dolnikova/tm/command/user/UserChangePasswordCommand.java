package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.util.HashUtil;

import java.security.MessageDigest;

public class UserChangePasswordCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public UserChangePasswordCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void setBootstrap(Bootstrap bootstrap) {
        super.setBootstrap(bootstrap);
    }

    @Override
    public String command() {
        return Constant.USER_CHANGE_PASSWORD;
    }

    @Override
    public String description() {
        return Constant.USER_CHANGE_PASSWORD_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(Constant.ENTER_OLD_PASSWORD);
        String oldPassword = "";
        while (oldPassword.isEmpty()) {
            oldPassword = Bootstrap.scanner.nextLine();
        }
        if (!bootstrap.getUser().getPassword().equals(HashUtil.stringToHashString(oldPassword))) {
            System.out.println(Constant.WRONG_PASSWORD);
            return;
        }

        System.out.println(Constant.ENTER_NEW_PASSWORD);
        String newPassword = "";
        while (newPassword.isEmpty()) {
            newPassword = Bootstrap.scanner.nextLine();
        }

        bootstrap.getUserService().merge(newPassword, bootstrap.getUser());
        System.out.println(Constant.PASSWORD_CHANGED_SUCCESSFULLY);
    }

    @Override
    public boolean isSecure() {
        return (!bootstrap.getUser().getUserId().isEmpty());
    }
}
