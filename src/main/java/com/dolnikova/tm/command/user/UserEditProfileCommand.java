package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.enumeration.DataType;

public class UserEditProfileCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public UserEditProfileCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void setBootstrap(Bootstrap bootstrap) {
        super.setBootstrap(bootstrap);
    }

    @Override
    public String command() {
        return Constant.USER_EDIT_PROFILE;
    }

    @Override
    public String description() {
        return Constant.USER_EDIT_PROFILE_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(Constant.ENTER_FIELD_NAME + "\n" + DataType.LOGIN + "\n" + DataType.ROLE);

        String fieldToEdit = "";
        while (fieldToEdit.isEmpty()) {
            fieldToEdit = Bootstrap.scanner.nextLine();
        }
        if (fieldToEdit.equals(DataType.LOGIN.toString())) {
            changeData(DataType.LOGIN);
            System.out.println(Constant.LOGIN_CHANGED);
        }
        if (fieldToEdit.equals(DataType.ROLE.toString())) {
            changeData(DataType.ROLE);
            System.out.println(Constant.ROLE_CHANGED);
        }

    }

    public void changeData(DataType dataType) {
        System.out.println(Constant.ENTER + dataType.toString());
        String newData = "";
        while (newData.isEmpty()) {
            newData = Bootstrap.scanner.nextLine();
        }
        bootstrap.getUserService().merge(newData, bootstrap.getUser(), dataType);
    }

    @Override
    public boolean isSecure() {
        return (!bootstrap.getUser().getUserId().isEmpty());
    }
}
