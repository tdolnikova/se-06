package com.dolnikova.tm.command.user;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;

public class UserFindProfileCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public UserFindProfileCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void setBootstrap(Bootstrap bootstrap) {
        super.setBootstrap(bootstrap);
    }

    @Override
    public String command() {
        return Constant.USER_FIND_PROFILE;
    }

    @Override
    public String description() {
        return Constant.USER_FIND_PROFILE_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("id: " + bootstrap.getUser().getUserId());
        System.out.println("login: " + bootstrap.getUser().getLogin());
        System.out.println("password: " + bootstrap.getUser().getPassword());
        System.out.println("role: " + bootstrap.getUser().getRole());
    }

    @Override
    public boolean isSecure() {
        return (!bootstrap.getUser().getUserId().isEmpty());
    }

}
