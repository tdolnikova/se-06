package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;

public class ProjectRemoveCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public String command() {
        return Constant.REMOVE_PROJECT;
    }

    @Override
    public String description() {
        return Constant.REMOVE_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        if (bootstrap.getProjectService().findAll().isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return;
        }
        System.out.println(Constant.WHICH_PROJECT_DELETE);
        Project project = null;
        while (project == null) {
            String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) return;
            project = bootstrap.getProjectService().findOne(projectName);
            if (project != null) bootstrap.getProjectService().remove(project);
            else System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
        }
        System.out.println(Constant.PROJECT_DELETED);
    }

    @Override
    public boolean isSecure() {
        return (!bootstrap.getUser().getUserId().isEmpty());
    }
}
