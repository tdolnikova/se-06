package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;

public class ProjectPersistCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public ProjectPersistCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public String command() {
        return Constant.PERSIST_PROJECT;
    }

    @Override
    public String description() {
        return Constant.PERSIST_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(Constant.INSERT_NEW_PROJECT_NAME);
        String projectName = Bootstrap.scanner.nextLine();
        if (projectName.isEmpty()) return;
        Project project = bootstrap.getProjectService().findOne(projectName);
        if (project != null) {
            System.out.println(Constant.PROJECT_NAME_ALREADY_EXIST);
            return;
        }
        Project newProject = new Project(projectName);
        newProject.setUserId(bootstrap.getUser().getUserId());
        bootstrap.getProjectService().persist(newProject);
        System.out.println(Constant.PROJECT + " " + projectName + " " + Constant.CREATED_M);
    }

    @Override
    public boolean isSecure() {
        return (!bootstrap.getUser().getUserId().isEmpty());
    }
}
