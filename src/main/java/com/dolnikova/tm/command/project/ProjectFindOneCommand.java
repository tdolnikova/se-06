package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;

public class ProjectFindOneCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public ProjectFindOneCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public String command() {
        return Constant.FIND_PROJECT;
    }

    @Override
    public String description() {
        return Constant.FIND_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        if (bootstrap.getProjectService().findAll().isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return;
        }
        System.out.println(Constant.INSERT_PROJECT_NAME);
        Project project = null;
        while (project == null) {
            String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = bootstrap.getProjectService().findOne(projectName);
            if (project == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
            else System.out.println(Constant.PROJECT_NAME + project.getName());
        }
    }

    @Override
    public boolean isSecure() {
        return (!bootstrap.getUser().getUserId().isEmpty());
    }
}
