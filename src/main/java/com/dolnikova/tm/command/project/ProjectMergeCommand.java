package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;

public class ProjectMergeCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public ProjectMergeCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public String command() {
        return Constant.MERGE_PROJECT;
    }

    @Override
    public String description() {
        return Constant.MERGE_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(Constant.INSERT_PROJECT_NAME);
        Project foundProject = null;
        while (foundProject == null) {
            String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) return;
            foundProject = bootstrap.getProjectService().findOne(projectName);
            if (foundProject == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + Constant.TRY_AGAIN);
            else {
                System.out.println(Constant.INSERT_NEW_PROJECT_NAME);
                boolean newNameInserted = false;
                while (!newNameInserted) {
                    String newProjectName = Bootstrap.scanner.nextLine();
                    if (newProjectName.isEmpty()) return;
                    bootstrap.getProjectService().merge(newProjectName, foundProject);
                    System.out.println(Constant.PROJECT_UPDATED);
                    newNameInserted = true;
                }
            }
        }
    }

    @Override
    public boolean isSecure() {
        return (!bootstrap.getUser().getUserId().isEmpty());
    }
}
