package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;

public class TaskPersistCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public TaskPersistCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public String command() {
        return Constant.PERSIST_TASK;
    }

    @Override
    public String description() {
        return Constant.PERSIST_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        Project project = findProject();
        if (project == null) return;
        System.out.println(Constant.INSERT_TASK);
        boolean taskCreationCompleted = false;
        while (!taskCreationCompleted) {
            String taskText = Bootstrap.scanner.nextLine();
            if (taskText.isEmpty()) taskCreationCompleted = true;
            else {
                Task newTask = new Task(project.getId(), taskText);
                newTask.setUserId(bootstrap.getUser().getUserId());
                bootstrap.getTaskService().persist(newTask);
                System.out.println(Constant.TASK + " " + taskText + " " + Constant.CREATED_F);
            }
        }
        System.out.println(Constant.TASK_ADDITION_COMPLETED);
    }

    public Project findProject() {
        if (bootstrap.getProjectService().findAll().isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return null;
        }
        System.out.println(Constant.CHOOSE_PROJECT);
        Project project = null;
        while (project == null) {
            String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = bootstrap.getProjectService().findOne(projectName);
            if (project == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
        }
        return project;
    }

    @Override
    public boolean isSecure() {
        return (!bootstrap.getUser().getUserId().isEmpty());
    }
}
