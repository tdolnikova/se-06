package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskFindOneCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public TaskFindOneCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public String command() {
        return Constant.FIND_TASK;
    }

    @Override
    public String description() {
        return Constant.FIND_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        Project project = findProject();
        if (project == null) return;
        List<Task> projectTasks = bootstrap.getTaskService().getTasksByProjectId(project.getId());
        System.out.println(Constant.IN_PROJECT + project.getName() + " " + projectTasks.size() + " " + Constant.OF_TASKS_WITH_POINT + ":");
        for (Task task : projectTasks) {
            System.out.println(task.getId());
        }
        System.out.println(Constant.INSERT_TASK_ID);
        Task task = null;
        while (task == null) {
            String taskId = Bootstrap.scanner.nextLine();
            if (taskId.isEmpty()) break;
            task = bootstrap.getTaskService().findOne(taskId);
            if (task == null) System.out.println(Constant.INCORRECT_NUMBER);
            else System.out.println(Constant.ID + Constant.COLON + taskId + " " + task.getName());
        }

    }

    public Project findProject() {
        if (bootstrap.getTaskService().findAll().isEmpty()) {
            System.out.println(Constant.NO_TASKS);
            return null;
        }
        if (bootstrap.getProjectService().findAll().isEmpty()) {
            System.out.println(Constant.NO_PROJECTS);
            return null;
        }
        System.out.println(Constant.CHOOSE_PROJECT);
        Project project = null;
        while (project == null) {
            String projectName = Bootstrap.scanner.nextLine();
            if (projectName.isEmpty()) break;
            project = bootstrap.getProjectService().findOne(projectName);
            if (project == null) System.out.println(Constant.PROJECT_NAME_DOESNT_EXIST + " " + Constant.TRY_AGAIN);
        }
        return project;
    }

    @Override
    public boolean isSecure() {
        return (!bootstrap.getUser().getUserId().isEmpty());
    }

}
