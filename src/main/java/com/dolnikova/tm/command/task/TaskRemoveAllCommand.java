package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Constant;

public class TaskRemoveAllCommand extends AbstractCommand {

    private Bootstrap bootstrap;

    public TaskRemoveAllCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public String command() {
        return Constant.REMOVE_ALL_TASKS;
    }

    @Override
    public String description() {
        return Constant.REMOVE_ALL_TASKS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        bootstrap.getTaskService().removeAll();
    }

    @Override
    public boolean isSecure() {
        return (!bootstrap.getUser().getUserId().isEmpty());
    }
}
