package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService implements IProjectService {

    private ProjectRepository projectRepository;
    private TaskService taskService;

    public ProjectService(ProjectRepository projectRepository, TaskService taskService) {
        this.projectRepository = projectRepository;
        this.taskService = taskService;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project findOne(String id) {
        if (projectRepository.findAll().isEmpty() || id.isEmpty()) return null;
        return projectRepository.findOne(id);
    }

    @Override
    public void persist(Project project) {
        if (project == null) return;
        projectRepository.persist(project);
    }

    @Override
    public void merge(String newProjectName, Project projectToMerge) {
        if (newProjectName.isEmpty() || projectToMerge == null) return;
        projectRepository.merge(newProjectName, projectToMerge);
    }

    @Override
    public void remove(Project project) {
        if (project == null) return;
        projectRepository.remove(project.getId());
        List<Task> projectTasks = taskService.getTasksByProjectId(project.getId());
        if (projectTasks.size() == 0) return;
        for (Task task : projectTasks) {
            taskService.remove(task);
        }
    }

    @Override
    public void removeAll() {
        projectRepository.removeAll();
        taskService.removeAll();
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

}
