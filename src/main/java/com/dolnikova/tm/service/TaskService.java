package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.repository.TaskRepository;

import java.util.List;

public class TaskService implements ITaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findOne(String id) {
        if (id.isEmpty()) return null;
        return taskRepository.findOne(id);
    }

    @Override
    public void persist(Task newTask) {
        if (newTask == null) return;
        taskRepository.persist(newTask);
    }

    @Override
    public void merge(String taskIdToUpdate, Task taskToAdd) {
        if (taskIdToUpdate.isEmpty() || taskToAdd == null) return;
        taskRepository.merge(taskIdToUpdate, taskToAdd);
    }

    @Override
    public void remove(Task task) {
        if (task != null) taskRepository.remove(task);
    }

    @Override
    public void removeAll() {
        taskRepository.removeAll();
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> getTasksByProjectId(String projectId) {
        return taskRepository.getTasksByProjectId(projectId);
    }

}
