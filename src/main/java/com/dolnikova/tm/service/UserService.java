package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumeration.DataType;
import com.dolnikova.tm.repository.UserRepository;

import java.util.List;
import java.util.UUID;

public class UserService implements IUserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findOne(String name) {
        if (name.isEmpty()) return null;
        return userRepository.findOne(name);
    }

    @Override
    public void persist(User user) {
        if (user == null) return;
        String userId = UUID.randomUUID().toString();
        user.setUserId(userId);
        userRepository.persist(user);
    }

    @Override
    public void merge(String newPassword, User entityToMerge) {
        if (newPassword.isEmpty() || entityToMerge == null) return;
        userRepository.merge(newPassword, entityToMerge);
    }

    public void merge(String data, User entityToMerge, DataType dataType) {
        if (data.isEmpty() || entityToMerge == null || dataType == null) return;
        userRepository.merge(data, entityToMerge, dataType);
    }

    @Override
    public void remove(User user) {
        userRepository.remove(user);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }
}
